// Should return a function that invokes `callback`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `callback` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(callback) {
  if (callback === undefined) {
    return null;
  } else {
    const cache = {};

    return (...argument) => {
      const arg = argument.join();

      if (arg in cache) {
        return cache[arg];
      } else {
        cache[arg] = callback(...argument);

        return cache[arg];
      }
    };
  }
}

module.exports = cacheFunction;
