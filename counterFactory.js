// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

const counterFactory = function () {
  let counter = 0;

  return {
    increment(value = 1) {
      return (counter += value);
    },

    decrement(value = 1) {
      return (counter -= value);
    },
  };
};

module.exports = counterFactory;
