// Should return a function that invokes `callback`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(callback, number) {
  return () => {
    if (callback === undefined || number === undefined || number <= 0) {
      return null;
    } else {
      number -= 1;

      return callback();
    }
  };
}

module.exports = limitFunctionCallCount;
