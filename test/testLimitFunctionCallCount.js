// Should return a function that invokes `callback`.
// The returned function should only allow `callback` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
const limitFunctionCallCount = require('../limitFunctionCallCount.js');

const callback = () => {
  // console.log('Hello World');
  return `Hello World`;
};

let limitedCallFunction = limitFunctionCallCount(callback, 3);

console.log(limitedCallFunction());
console.log(limitedCallFunction());
console.log(limitedCallFunction());
console.log(limitedCallFunction());
console.log(limitedCallFunction());
