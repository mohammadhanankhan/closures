const counterFactory = require('../counterFactory');

const counter = counterFactory();

console.log(counter.increment());

const secondCounter = counterFactory();

console.log(secondCounter.increment());
