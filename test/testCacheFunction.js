const cacheFunction = require('../cacheFunction');

const callback = number => {
  console.log('Taking 10 seconds..');

  return number;
};
const cache = cacheFunction(callback);

console.log(cache(5));
console.log(cache(10));
console.log(cache(5));

function squareCallback(number) {
  console.log(`Obtaining the square of ${number}.`);

  return number * number;
}
const secondCache = cacheFunction(squareCallback);

console.log(secondCache(2));
console.log(secondCache(4));
console.log(secondCache(2));

function sumCallback(...numbers) {
  console.log(`Obtaining the sum of ${numbers}.`);

  return numbers.reduce((a, b) => a + b);
}

const thirdCache = cacheFunction(sumCallback);

console.log(thirdCache(2, 3, 4));
console.log(thirdCache(2, 5, 7));
console.log(thirdCache(1, 3, 4));
